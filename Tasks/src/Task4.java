import java.util.Scanner;
import java.lang.Math;

public class Task4 {
    public static void  main(String[]args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextFloat();
        double b = scanner.nextFloat();
        double h = Math.sqrt(a*a+b*b);
        double p = a+b+h;
        double ar = 0.5 * a * b;
        System.out.println("Perimeter = "+p+","+"Area = "+ar);
    }
}
